#!/usr/bin/env python
# encoding: utf-8
import numpy
from distutils.core import setup
from Cython.Build import cythonize

setup(
    name = "cflux",
    include_dirs = [numpy.get_include()],
    ext_modules = cythonize('cflux.pyx'),
)
