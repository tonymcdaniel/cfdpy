#!/usr/bin/env python
# encoding: utf-8
"""
File Name : logger.py

Author    : Tony McDaniel
Contact   : tony@tonymcdaniel.com

Logging functions.

Creation Date : Wed Feb 12, 2014  05:37PM
Last Modified : Fri Mar 14, 2014  01:59PM

Copyright (c) 2014 Tony R. McDaniel

Released under the terms of the GNU General Public License.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import division


class Logger(object):

    """Docstring for Logger. """

    def __init__(self,prefix='LOG : ',logfile=None,filemode='w',console=True):
        """Initialize Logger object with given prefix and logfile.

        :prefix: Text to precede log messages.
        :logfile: Text file for output.
        :filemode: Determines whether to overwrite or append to existing log.
        :console: Echos messages to stdout if True.

        """
        self._prefix   = prefix
        self._logfile  = logfile
        self._filemode = filemode
        self._console  = console
        self._f        = None

        if self._logfile:
            self.start()

    def __call__(self,message,vals=None):
        self.log(message,vals)

    def log(self,message,vals=None):
        if self._console:
            print(self._prefix+message)
            if vals:
                for v in vals:
                    print(self._prefix+repr(v))

        if self._f:
            self._f.write(self._prefix+message+'\n')
            if vals:
                for v in vals:
                    self._f.write(self._prefix+repr(v)+'\n')

    def line(self,ncols=60):
        self.log("-"*ncols)

    def start(self):
        if self._logfile and self._f is None:
            try:
                self._f = open(self._logfile,self._filemode)
            except:
                print("LOGGER : Unable to open "+self._logfile+" for writing.")

    def restart(self):
        if self._logfile and self._f is None:
            try:
                self._f = open(self._logfile,'a')
            except:
                print("LOGGER : Unable to open "+self._logfile+" for appending.")

    def stop(self):
        if self._logfile:
            self._f.close()
            self._f = None
            print("LOGGER : Logging has stopped.")


if __name__ == '__main__':
    """
    Try a couple of test calls.
    """
    var1 = ['a','b','c']
    var2 = [1,2,3]
    # First test console output
    f = Logger(prefix='TEST1 : ')
    f.log("Basic Message")
    f.log("String Message",var1)
    f.log("Number Message",var2)
    f.log("Var2[1] = %d"%var2[1])
    # Now test file output
    f = Logger(prefix='TEST2 : ',logfile='test.out')
    f.log("Basic Message")
    f.log("String Message",var1)
    f.log("Number Message",var2)
    f.stop()

