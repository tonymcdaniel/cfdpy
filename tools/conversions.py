#!/usr/bin/env python
# encoding: utf-8
"""
File Name : conversions.py

Author    : Tony McDaniel
Contact   : tony@tonymcdaniel.com

Miscellaneous conversion functions

Creation Date : Thu Mar 13, 2014  11:30AM
Last Modified : Thu Mar 13, 2014  11:48AM

Copyright (c) 2014 Tony R. McDaniel

Released under the terms of the GNU General Public License.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

def conserved_to_primitive(Q,gamma=1.4):
    """
    Computes the primitive variables
    q = [ rho, u, v, p ]
    from the conserved variables
    Q = [ rho, rho*u, rho*v, E ]
    """
    q = np.zeros_like(Q)
    if len(Q.shape) == 1:
        # density
        q[0] = Q[0]
        # x-velocity
        q[1] = Q[1]/Q[0]
        # y-velocity
        q[2] = Q[2]/Q[0]
        # pressure
        q[3] = (gamma-1)*(Q[3]-0.5*Q[0]*(q[1]**2+q[2]**2))
    else:
        # density
        q[:,0] = Q[:,0]
        # x-velocity
        q[:,1] = Q[:,1]/Q[:,0]
        # y-velocity
        q[:,2] = Q[:,2]/Q[:,0]
        # pressure
        q[:,3] = (gamma-1)*(Q[:,3]-0.5*Q[:,0]*(q[:,1]**2+q[:,2]**2))
    return q

def main():
    Q = np.array([ 1.2, 2.4, 3.6, 4.0 ])
    print("Q = ")
    print(Q)
    print("q = ")
    print(conserved_to_primitive(Q,1.4))
    print("")
    Q = np.array([[ 1.2, 2.4, 3.6, 4.0 ],[ 1.0, 2.0, 3.0, 4.0 ]])
    print("Q = ")
    print(Q)
    print("q = ")
    print(conserved_to_primitive(Q,1.4))

if __name__ == '__main__':
    main()

