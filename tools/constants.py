#!/usr/bin/env python
# encoding: utf-8
"""
File Name : constants.py

Author    : Tony McDaniel
Contact   : tony@tonymcdaniel.com

Various bits of data that are used throughout the code.

Creation Date : Thu Mar 13, 2014  11:12PM
Last Modified : Thu Mar 27, 2014  11:55AM

Copyright (c) 2014 Tony R. McDaniel

Released under the terms of the GNU General Public License.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""

vtk_types                = {'tri':5,'quad':9,'tet':10,'hex':12,'prism':13,'pyramid':14}
nodes_per_element        = {'tri':3,'quad':4,'tet':4,'hex':8,'prism':6,'pyramid':5}
names_euler_2d_conserved = [ 'density','x-momentum','y-momentum','total-energy' ]
names_euler_3d_conserved = [ 'density','x-momentum','y-momentum','z-momentum','total-energy' ]
names_euler_2d_primitive = [ 'density','x-velocity','y-velocity','pressure' ]
names_euler_3d_conserved = [ 'density','x-velocity','y-velocity','z-velocity','pressure' ]

