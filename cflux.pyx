#!/usr/bin/env python
# from math import sqrt
from libc.math cimport sqrt
import numpy as np
cimport numpy as np
cimport cython

@cython.boundscheck(False)
@cython.cdivision(True)
def fluxVanLeer(double[:] Ql, double[:] Qr, double nx, double ny, double gamma=1.4):
    """
    Compute the flux of $Q$ across the boundary with unit normal $n$
    """
    cdef np.ndarray[double,ndim=1] Fp,Fm
    # cdef double gamma = 1.4
    # normalize the normal vector
    cdef double length = sqrt(nx**2+ny**2)
    nx = nx/length
    ny = ny/length
    # values for left side
    cdef double rl,ul,vl,el,pl,cl,Ul
    rl = Ql[0]
    ul = Ql[1]/rl
    vl = Ql[2]/rl
    el = Ql[3]  # e is J per unit volume
    pl = (gamma-1.)*(el - 0.5*rl*(ul**2+vl**2))
    cl = sqrt(gamma*pl/rl)
    Ul = (ul*nx + vl*ny)
    # values for left side
    cdef double rr,ur,vr,er,pr,cr,Ur
    rr = Qr[0]
    ur = Qr[1]/rr
    vr = Qr[2]/rr
    er = Qr[3]  # e is J per unit volume
    pr = (gamma-1.)*(er - 0.5*rr*(ur**2+vr**2))
    cr = sqrt(gamma*pr/rr)
    Ur = (ur*nx + vr*ny)
    cdef double f1,f1p,f2p,f1m,f2m
    if Ul > cl:
        Fm = np.zeros(4)
        f1 = rl*Ul
        Fp = length*np.array([f1, f1*ul+pl*nx, f1*vl+pl*ny, (el+pl)*Ul])
    elif Ur < -cr:
        Fp = np.zeros(4)
        f1 = rr*Ur
        Fm = length*np.array([f1, f1*ur+pr*nx, f1*vr+pr*ny, (er+pr)*Ur])
    else:
        f1p = 0.25*rl*cl*((Ul/cl)+1.)**2
        f2p = -Ul+2.*cl
        f1m = -0.25*rr*cr*((Ur/cr)-1.)**2
        f2m = -Ur-2.*cr
        Fp = length*np.array([f1p,
                           f1p*(nx*f2p/gamma+ul),
                           f1p*(ny*f2p/gamma+vl),
                           f1p*((f2p*Ul*(gamma-1.)+2.*cl**2)/(gamma**2-1.)+0.5*(ul**2+vl**2))])
        Fm = length*np.array([f1m,
                           f1m*(nx*f2m/gamma+ur),
                           f1m*(ny*f2m/gamma+vr),
                           f1m*((f2m*Ur*(gamma-1.)+2.*cr**2)/(gamma**2-1.)+0.5*(ur**2+vr**2))])
    return Fp, Fm


@cython.boundscheck(False)
@cython.cdivision(True)
def jacobianFD(double[:] Ql, double[:] Qr, double nx, double ny, double gamma=1.4, double dQ=1e-5):
    cdef np.ndarray[double,ndim=2] Jp,Jm
    cdef np.ndarray[double,ndim=1] Fp,Fm,dFp,dFm
    cdef double[:] Qlp,Qrp
    Jp = np.zeros((4, 4))
    Jm = np.zeros((4, 4))
    Fp,Fm = fluxVanLeer(Ql, Qr, nx, ny)
    for j in range(4):
        Qlp = Ql.copy()
        Qrp = Qr.copy()
        Qlp[j] += dQ
        Qrp[j] += dQ
        dFp, dFm = fluxVanLeer(Qlp, Qrp, nx, ny)
        Jp[:, j] = (dFp - Fp)/dQ
        Jm[:, j] = (dFm - Fm)/dQ
    return Jp, Jm


@cython.boundscheck(False)
@cython.cdivision(True)
def c_residual(long[:,:] elements, double[:,:,:] normals, double[:,:] Q, np.ndarray[double,ndim=1] R):
    """
    Compute the residual vector for the RHS.
    """
    cdef int nnode,nelem,ndof,npelem
    nnode = Q.shape[0]
    ndof = Q.shape[1]
    nelem = elements.shape[0]
    npelem = elements.shape[1]
    # hard coding these to simplify the function call
    # TODO will need to fix for general grids
    # npelem = 3
    # Initialize R to 0.0
    # cdef np.ndarray[double,ndim=1] R
    # R = np.zeros(ndof*nnode)

    # Loop over interior elements
    cdef int t,i
    cdef long pl,pr
    cdef double[:] ql,qr
    cdef double nx,ny,length
    cdef np.ndarray[double,ndim=1] fp,fm

    for t in range(nelem):
        for i in range(npelem):
            pl = elements[t,i]
            pr = elements[t,(i+1)%npelem]
            ql = Q[pl]
            qr = Q[pr]
            nx = normals[t,i,0]
            ny = normals[t,i,1]
            length = np.sqrt(nx**2+ny**2)
            # TODO get gamma as a param
            fp,fm = fluxVanLeer(ql,qr,nx,ny)
            R[ndof*pl:ndof*(pl+1)] += fp
            R[ndof*pl:ndof*(pl+1)] += fm
            R[ndof*pr:ndof*(pr+1)] -= fp
            R[ndof*pr:ndof*(pr+1)] -= fm

    # return R

@cython.boundscheck(False)
@cython.cdivision(True)
def c_rhs_boundary(int tag,long[:,:] elm, double[:,:] nodes, double[:] p, double[:,:] Q, double[:] freeQ, np.ndarray[double,ndim=1] R):
    cdef int nelem,ndof,e,pl,pr
    cdef double nx,ny
    cdef double gamma = 1.4
    cdef double[:] ql,qr
    cdef np.ndarray[double,ndim=1] f,fp,fm

    nelem = elm.shape[0]
    ndof = Q.shape[1]

    if tag == 0: #'wall':
        for e in range(nelem):
            pl = elm[e,0]
            pr = elm[e,1]
            ny = -(nodes[pr,0] - nodes[pl,0])
            nx = nodes[pr,1] - nodes[pl,1]

            # Left side of face
            f = np.array([ 0., nx*(0.75*p[pl] + 0.25*p[pr]), ny*(0.75*p[pl] + 0.25*p[pr]), 0.])
            R[ndof*pl:ndof*(pl+1)] += 0.5*f
            # Right side of face
            f = np.array([ 0., nx*(0.75*p[pr] + 0.25*p[pl]), ny*(0.75*p[pr] + 0.25*p[pl]), 0.])
            R[ndof*pr:ndof*(pr+1)] += 0.5*f

    elif tag == 1: #'freestream':
        for e in range(nelem):
            pl = elm[e,0]
            pr = elm[e,1]
            ql = Q[pl]
            qr = Q[pr]
            ny = -(nodes[pr,0] - nodes[pl,0])
            nx = nodes[pr,1] - nodes[pl,1]
            # Left side of face
            fp,fm = fluxVanLeer(ql,freeQ,nx,ny,gamma)
            R[ndof*pl:ndof*(pl+1)] += 0.5*(fp + fm)
            # Right side of face
            fp,fm = fluxVanLeer(freeQ,qr,nx,ny,gamma=gamma)
            R[ndof*pr:ndof*(pr+1)] += 0.5*(fp + fm)
