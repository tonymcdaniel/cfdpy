#!/usr/bin/env python
# encoding: utf-8
"""
File Name : plot.py

Author    : Tony McDaniel
Contact   : tony@tonymcdaniel.com

%DESCRIPTION%

Creation Date : Wed Mar 05, 2014  05:26PM
Last Modified : Wed Mar 05, 2014  06:50PM

Copyright (c) 2014 Tony R. McDaniel

Released under the terms of the GNU General Public License.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import division
import numpy as np
import matplotlib.pyplot as plt

def triangulate(x,y,t=None):
    import matplotlib.tri as tri
    if t is None:
        T = tri.Triangulation(x,y)
    else:
        T = tri.Triangulation(x,y,t)
    return T


def plot_contour(x,y,z,tri=None,outfile=None,title=None):
    T = triangulate(x,y,tri)
    plt.figure()
    plt.gca().set_aspect('equal')
    plt.triplot(T,alpha=0.2)
    plt.tricontourf(T,z,50)
    plt.colorbar()
    if title is not None:
        plt.title(title)
    if outfile is not None:
        plt.savefig(outfile)
    else:
        plt.show()

def main():
    pass

if __name__ == '__main__':
    main()

