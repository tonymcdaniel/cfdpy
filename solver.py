#!/usr/bin/env python
# encoding: utf-8
"""
File Name : solver.py

Author    : Tony McDaniel
Contact   : tony@tonymcdaniel.com

Implements a 2D flow solver class.

Creation Date : Sat Mar 01, 2014  06:56PM
Last Modified : Sun Dec 11, 2016  05:37PM

Copyright (c) 2014 Tony R. McDaniel

Released under the terms of the GNU General Public License.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import division
import numpy as np
import numpy.linalg as la
from pysparse.sparse import spmatrix
from pysparse.itsolvers import gmres
from pysparse.precon import precon
import matplotlib.pyplot as plt
from time import time,strftime
from datetime import timedelta
import sys
from tools.logger import Logger
from math import *
from grid import Grid
# from plot import plot_contour
from cflux import *
from tools.conversions import *
from tools.constants import *

def save_restart(solver):
    import cPickle as pickle
    outfile = '%s.%d.restart'%(solver.base_name,solver.count)
    solver.log("Writing restart file to "+outfile)
    with open(outfile,'wb') as f:
        pickle.dump(solver.Q,f)

def load_restart(restartfile,solver):
    import cPickle as pickle
    print("Loading restart file from "+restartfile)
    with open(restartfile,'rb') as f:
        solver.Q = pickle.load(f)

class Solver(object):

    """2D Finite Volume Solver"""

    def __init__(self,config=None):
        """@todo: to be defined1.

        :config: Configuration file.

        """
        self._config   = config # Config file
        self._gridfile = None   # Input grid in SU2 format
        self._save_restart  = False
        self._restart  = False
        self.debug     = 0      # Set to 1 for verbose output
        self.A         = None   # Jacobian matrix (dR/dQ)
        self.R         = None   # Residual vector
        self.Q         = None   # Solution vector
        self.dt        = None   # Time steps
        if config:
            self.load(config)


    def load(self,config):
        self._config = config
        self._parse_config(config)
        self._initialize()

    def add_source(self,source):
        self.source = source

    def step(self):
        # compute local time-steps
        self.secondary_variables()
        self._timestep()
        self.log(" CFL start = %5.3f  max = %5.3f  steps = %d"%(self.cfl,self.cfl_max,self.cfl_steps))
        self.log(" Min ∆t = %5.4e   Max ∆t = %5.4e "%(1./np.max(self.dt),1./np.min(self.dt)))
        self.log(" GMRES maxiter = %d  tolerance = %4.3e "%(self.lin_maxiter,self.lin_tol))
        self.log.line()
        self.log("                                             GMRES        ")
        self.log(" Step  CFL      Residual       Time      Iter  Residual   ")
        self.log.line()
        self.residual = 1e9
        self.count = 0
        global_start = time()
        while (self.residual > self.tol):
            if (self.maxiter is not None) and (self.count > self.maxiter):
                break
            start = time()
            self.secondary_variables() # compute u,v,p,c at each node
            self._jacobian()           # compute dR/dQ
            self._residual()           # compute R
            self._timestep()           # compute delta t at each node
            self._apply_timestep()     # apply temporal damping to Jacobian matrix
            # self.dQ,info = gmres(csr_matrix(self.A),self.R,x0=self.dQ,tol=1e-6)
            # linear solve of dR/dQ * deltaQ = R
            K = precon.jacobi(self.A)
            info,itcount,relres = gmres(self.A.to_csr(),self.R,self.dQ,self.lin_tol,self.lin_maxiter,K)
            # info,itcount,relres = gmres(self.A.to_csr(),self.R,self.dQ,self.lin_tol,self.lin_maxiter)
            self.residual = la.norm(self.R)
            # self.residual = la.norm(self.dQ)
            end = time()
            self.count += 1
            self.log("%5d  %2.1e  %8.7e  %2.1e  %5d  %5.3e"%
                     (self.count,self.cfl,self.residual,end-start,itcount,relres))
            if self.debug:
                print "dq = "
                print self.dQ
            # apply dQ to solution
            self.Q -= self.dQ.reshape(self.Q.shape)
            # update the CFL with ramping
            if self.cfl < self.cfl_max:
                self.cfl += self.cfl_delta
        global_end = time()
        # compute the secondary variables for plotting
        self.log.line()
        self.log("Run completed at "+strftime("%c"))
        self.log("Total run time = "+str(timedelta(seconds=(global_end-global_start))))
        if self._save_restart:
            save_restart(self)
        self.secondary_variables()


    def secondary_variables(self):
        """
        Computes the secondary variables at each node using
        the vector of conserved variables
        Q = [rho, rho*u, rho*v, E]
        """
        # edge length for normal vectors
        self.length = np.sqrt(self.grid.normals[:,:,0]**2+self.grid.normals[:,:,1]**2)
        # velocity in x and y directions
        self.u = self.Q[:,1]/self.Q[:,0]
        self.v = self.Q[:,2]/self.Q[:,0]
        # velocity normal to control volume face u*nx + v*ny
        # self.U = self.u*self.grid.normals[:,:,0] + self.v*self.grid.normals[:,:,1]
        # pressure
        self.p = self.gm1*(self.Q[:,3] - 0.5*self.Q[:,0]*(self.u**2 + self.v**2))
        # speed of sound
        self.c = np.sqrt(self.gamma*self.p/self.Q[:,0])
        # mach number
        self.m = np.sqrt(self.u**2 + self.v**2)/self.c


    def _parse_config(self,config):
        """
        Read and parse the configuration file.
        """
        COMMENT_CHAR = '#'
        OPTION_CHAR =  '='
        self.options = {}
        string_opts = ['grid','logfile','plotfile']
        float_opts  = ['cfl','cfl_max','rho','u','v','gamma','p','mach','alpha','tolerance','lin_tol']
        int_opts    = ['ndof','cfl_steps','verbose','lin_maxiter']
        print("SOLVER : Reading configuration from "+config)
        with open(config,'r') as f:
            header = f.readline()
            if header.split()[0] != 'cfdpy':
                print("ERROR : Config file is not in the correct format")
                sys.exit(1)
            for line in f:
                if COMMENT_CHAR in line:
                    # split on comment char, keep only the part before
                    line, comment = line.split(COMMENT_CHAR, 1)
                # Second, find lines with an option=value:
                if OPTION_CHAR in line:
                    # split on option char:
                    option, value = line.split(OPTION_CHAR, 1)
                    # strip spaces:
                    option = option.strip()
                    value = value.strip()
                    if option in float_opts:
                        self.options[option] = float(value)
                    elif option in int_opts:
                        self.options[option] = int(value)
                    else:
                        self.options[option] = value


    def _initialize(self):
        # OUTPUT FILES
        self.base_name = self._config.rsplit('.cfg',1)[0]
        # set up the log file
        if 'logfile' in self.options:
            self.log = Logger('SOLVER : ',self.options['logfile'])
        else:
            self.log = Logger('SOLVER : ',self.base_name+'.log')
        self.log("Run started at "+strftime("%c"))
        self.log("Using configuration "+self._config)
        # set up the plot file
        if 'plotfile' in self.options:
            self.plotfile = self.options['plotfile']
        else:
            self.plotfile = self.base_name
        if 'restart' in self.options:
            self._restart = True
            self._restart_file = self.options['restart_file']
        if 'save_restart' in self.options:
            self._save_restart = True

        # FREESTREAM VARIABLES
        # initialize the freestream variables
        # Density
        if 'rho' in self.options:
            rho = self.options['rho']
        else:
            rho = 1.0

        # Velocity
        if 'mach' in self.options:
            if 'alpha' in self.options:
                alpha = self.options['alpha']
            else:
                alpha = 0.0
            u = self.options['mach']*np.cos(np.radians(alpha))
            v = self.options['mach']*np.sin(np.radians(alpha))
        else:
            if 'u' in self.options:
                u = self.options['u']
            else:
                u = 1.0

            if 'v' in self.options:
                v = self.options['v']
            else:
                v = 0.0

        # Specific heat ratio
        if 'gamma' in self.options:
            gamma = self.options['gamma']
        else:
            gamma = 1.4
        self.gamma = gamma
        self.gm1 = gamma - 1.
        self.gm1g = self.gm1/gamma
        self.gp1 = gamma + 1.
        self.gp1g = self.gp1/gamma

        # Pressure
        if 'p' in self.options:
            p = self.options['p']
        else:
            p = 1./gamma

        self.freestreamQ = np.array([rho, rho*u, rho*v, p/self.gm1+0.5*rho*(u**2+v**2)])
        self.log.line()
        self.log("Freestream conditions are")
        self.log("  Density    = %f"%rho)
        self.log("  X-velocity = %f"%u)
        self.log("  Y-velocity = %f"%v)
        self.log("  Gamma      = %f"%gamma)
        self.log("  Pressure   = %f"%p)
        self.log.line()

        # GRID AND SOLUTION INITIALIZATION
        # initialize the grid
        if 'ndof' in self.options:
            self.ndof = self.options['ndof']
        else:
            self.ndof = 4

        if 'grid' in self.options:
            self.grid = Grid(self.options['grid'])
        else:
            self.grid = Grid()

        self.area = np.zeros(self.grid.nnode)
        self.dQ = np.zeros(self.grid.nnode*self.ndof)
        if self._restart:
            load_restart(self._restart_file,self)
        else:
            self.Q = np.zeros((self.grid.nnode,self.ndof))
            for i in xrange(self.grid.nnode):
                self.Q[i,:] = self.freestreamQ

        # SOLVER OPTIONS
        # configure CFL
        if 'cfl' in self.options:
            self.cfl = self.options['cfl']
        else:
            self.cfl = 1.0
        if 'cfl_max' in self.options:
            self.cfl_max = self.options['cfl_max']
        else:
            self.cfl_max = self.cfl
        if 'cfl_steps' in self.options:
            self.cfl_steps = self.options['cfl_steps']
        else:
            self.cfl_steps = 10
        self.cfl_delta = (self.cfl_max - self.cfl)/float(self.cfl_steps)

        # tolerance and maxiterations
        if 'tolerance' in self.options:
            self.tol = self.options['tolerance']
        else:
            self.tol = 1e-6
        if 'maxiter' in self.options:
            self.maxiter = self.options['maxiter']
        else:
            self.maxiter = None
        if 'lin_maxiter' in self.options:
            self.lin_maxiter = self.options['lin_maxiter']
        else:
            self.lin_maxiter = 2000
        if 'lin_tol' in self.options:
            self.lin_tol = self.options['lin_tol']
        else:
            self.lin_tol = 1e-5

        # set debug flag for verbosity
        if 'verbose' in self.options:
            self.debug = self.options['verbose']


    def _timestep(self):
        """
        Compute the local timestep for each node.
        """
        # Set dt to zero
        self.dt = np.zeros(self.grid.nnode)
        # if self.dt is None:
        #     self.dt = np.zeros(self.grid.nnode)
        # else:
        #     self.dt *= 0.0
        # Loop over interior elements
        for t in xrange(self.grid.nelem):
            for i in xrange(self.grid.npelem):
                pl = self.grid.elements[t,i]
                pr = self.grid.elements[t,(i+1)%self.grid.npelem]
                ql = self.Q[pl];  qr = self.Q[pr]
                ul = ql[1]/ql[0]; ur = qr[1]/qr[0]
                vl = ql[2]/ql[0]; vr = qr[2]/qr[0]
                cl = self.c[pl];  cr = self.c[pr]
                u = 0.5*(ul+ur)
                v = 0.5*(vl+vr)
                c = 0.5*(cl+cr)
                nx = self.grid.normals[t,i,0]
                ny = self.grid.normals[t,i,1]
                rl = np.sqrt(nx**2+ny**2)
                Vn = fabs(u*nx+v*ny) + c*rl
                self.dt[pl] += Vn
                self.dt[pr] += Vn

        # Loop over boundary elements
        for tag,elm in self.grid.boundaries.iteritems():
            if self.debug:
                print "Processing wall boundary."
            for e in elm:
                pl = e[0];          pr = e[1]
                ql = self.Q[pl];    qr = self.Q[pr]
                ul = ql[1]/ql[0];   ur = qr[1]/qr[0]
                vl = ql[2]/ql[0];   vr = qr[2]/qr[0]
                cl = self.c[pl];    cr = self.c[pr]
                ny = -(self.grid.nodes[pr,0] - self.grid.nodes[pl,0])
                nx = self.grid.nodes[pr,1] - self.grid.nodes[pl,1]
                rl = np.sqrt(nx**2+ny**2)
                # Left side of face
                u = 0.75*ul + 0.25*ur
                v = 0.75*vl + 0.25*vr
                c = 0.75*self.c[pl] + 0.25*self.c[pr]
                Vn = fabs(u*nx+v*ny) + c*rl
                self.dt[pl] += 0.5*Vn # divide by 2 since normal is for face
                # Right side of face
                u = 0.75*ur + 0.25*ul
                v = 0.75*vr + 0.25*vl
                c = 0.75*self.c[pr] + 0.25*self.c[pl]
                Vn = fabs(u*nx+v*ny) + c*rl
                self.dt[pr] += 0.5*Vn # divide by 2 since normal is for face

    def _apply_timestep(self):
        # Loop over nodes and apply temporal damping
        for i in xrange(self.grid.nnode):
            if self.debug:
                print self.A[self.ndof*i:self.ndof*(i+1),self.ndof*i:self.ndof*(i+1)]
                print np.eye(self.ndof)*self.dt[i]/self.cfl
            irange = range(self.ndof*i,self.ndof*(i+1))
            self.A.update_add_mask(np.eye(self.ndof)*self.grid.area[i]*self.dt[i]/(self.cfl),irange,irange,[True]*self.ndof,[True]*self.ndof)
            # self.A[self.ndof*i:self.ndof*(i+1),self.ndof*i:self.ndof*(i+1)] += np.eye(self.ndof)*self.dt[i]/self.cfl




    def _residual(self):
        """
        Compute the residual vector for the RHS.
        """
        # Initialize R to 0.0
        if self.R is None:
            self.R = np.zeros(self.ndof*self.grid.nnode)
        else:
            self.R *= 0.0

        try:
            self.R -= self.source.reshape(self.R.shape)
        except:
            pass

        c_residual(self.grid.elements, self.grid.normals, self.Q, self.R)
        for tag,elm in self.grid.boundaries.iteritems():
            if tag == 'wall':
                c_rhs_boundary(0,elm,self.grid.nodes, self.p, self.Q, self.freestreamQ, self.R)
            elif tag == 'freestream':
                c_rhs_boundary(1,elm,self.grid.nodes, self.p, self.Q, self.freestreamQ, self.R)
            else:
                print("Unknown boundary type : %s"%tag)



    def _jacobian(self):
        self.A = spmatrix.ll_mat(self.ndof*self.grid.nnode,self.ndof*self.grid.nnode)
        mask = [True]*self.ndof

        for t in xrange(self.grid.nelem):
            for i in xrange(self.grid.npelem):
                pl = self.grid.elements[t,i]
                pr = self.grid.elements[t,(i+1)%self.grid.npelem]
                ql = self.Q[pl]
                qr = self.Q[pr]
                nx = self.grid.normals[t,i,0]
                ny = self.grid.normals[t,i,1]
                Jp,Jm = jacobianFD(ql,qr,nx,ny,gamma=self.gamma)
                lrange = range(self.ndof*pl,self.ndof*(pl+1))
                rrange = range(self.ndof*pr,self.ndof*(pr+1))
                # left diagonal
                self.A.update_add_mask(Jp.T,lrange,lrange,mask,mask)
                # self.A[self.ndof*pl:self.ndof*(pl+1),self.ndof*pl:self.ndof*(pl+1)] += Jp
                # row left, column right
                self.A.update_add_mask(Jm.T,lrange,rrange,mask,mask)
                # self.A[self.ndof*pl:self.ndof*(pl+1),self.ndof*pr:self.ndof*(pr+1)] += Jm
                # row right, column left
                self.A.update_add_mask(-Jp.T,rrange,lrange,mask,mask)
                # self.A[self.ndof*pr:self.ndof*(pr+1),self.ndof*pl:self.ndof*(pl+1)] -= Jp
                # right diagonal
                self.A.update_add_mask(-Jm.T,rrange,rrange,mask,mask)
                # self.A[self.ndof*pr:self.ndof*(pr+1),self.ndof*pr:self.ndof*(pr+1)] -= Jm

        for tag,elm in self.grid.boundaries.iteritems():
            if tag == 'wall':
                if self.debug:
                    print "Processing wall boundary."
                for e in elm:
                    pl = e[0]
                    pr = e[1]
                    ql = self.Q[pl]
                    qr = self.Q[pr]
                    ul = ql[1]/ql[0]
                    vl = ql[2]/ql[0]
                    ur = qr[1]/qr[0]
                    vr = qr[2]/qr[0]
                    gamma = self.gamma
                    ny = -(self.grid.nodes[pr,0] - self.grid.nodes[pl,0])
                    nx = self.grid.nodes[pr,1] - self.grid.nodes[pl,1]
                    Jp = 0.75*(gamma-1.)*np.array([[ 0., 0., 0., 0.],
                                             [ 0.5*nx*(ul**2+vl**2), -nx*ul, -nx*vl, nx ],
                                             [ 0.5*ny*(ul**2+vl**2), -ny*ul, -ny*vl, ny ],
                                             [ 0., 0., 0., 0.]])
                    if self.debug:
                        print "pl = ",pl; print "pr = ",pr; print "nx = ",nx; print "ny = ",ny; print "Jp = ",Jp
                    Jm = 0.25*(gamma-1.)*np.array([[ 0., 0., 0., 0.],
                                             [ 0.5*nx*(ur**2+vr**2), -nx*ur, -nx*vr, nx ],
                                             [ 0.5*ny*(ur**2+vr**2), -ny*ur, -ny*vr, ny ],
                                             [ 0., 0., 0., 0.]])
                    if self.debug:
                        print "Jm = ",Jm
                    lrange = range(self.ndof*pl,self.ndof*(pl+1))
                    rrange = range(self.ndof*pr,self.ndof*(pr+1))
                    self.A.update_add_mask(Jp.T,lrange,lrange,mask,mask)
                    self.A.update_add_mask(Jm.T,rrange,rrange,mask,mask)
                    # self.A[self.ndof*pl:self.ndof*(pl+1),self.ndof*pl:self.ndof*(pl+1)] += Jp
                    # self.A[self.ndof*pr:self.ndof*(pr+1),self.ndof*pr:self.ndof*(pr+1)] += Jm

            elif tag == 'freestream':
                if self.debug:
                    print "Processing freestream boundary."
                for e in elm:
                    pl = e[0]
                    pr = e[1]
                    ql = self.Q[pl]
                    qr = self.Q[pr]
                    ny = -(self.grid.nodes[pr,0] - self.grid.nodes[pl,0])
                    nx = self.grid.nodes[pr,1] - self.grid.nodes[pl,1]
                    Jp,Jm = jacobianFD(ql,self.freestreamQ,nx,ny,gamma=self.gamma)
                    if self.debug:
                        print "ql = ",ql; print "qr = ",qr; print "pl = ",pl; print "pr = ",pr;
                        print "nx = ",nx; print "ny = ",ny
                    lrange = range(self.ndof*pl,self.ndof*(pl+1))
                    self.A.update_add_mask(0.5*Jp.T,lrange,lrange,mask,mask)
                    # self.A[self.ndof*pl:self.ndof*(pl+1),self.ndof*pl:self.ndof*(pl+1)] += 0.5*Jp
                    Jp,Jm = jacobianFD(self.freestreamQ,qr,nx,ny,gamma=self.gamma)
                    rrange = range(self.ndof*pr,self.ndof*(pr+1))
                    self.A.update_add_mask(0.5*Jm.T,rrange,rrange,mask,mask)
                    # self.A[self.ndof*pr:self.ndof*(pr+1),self.ndof*pr:self.ndof*(pr+1)] += 0.5*Jm
            else:
                print("Unknown boundary type : %s"%tag)




def main():
    from vtk_out import writeVTK
    import sys
    config = sys.argv[1]
    s = Solver(config)
    s.step()
    writeVTK(s.plotfile,s.grid,conserved_to_primitive(s.Q),names=names_euler_2d_primitive)


if __name__ == '__main__':
    main()

