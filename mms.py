#!/usr/bin/env python
# encoding: utf-8
"""
File Name : mms.py

Author    : Tony McDaniel
Contact   : tony@tonymcdaniel.com

Manufactured solutions for code verification.

Creation Date : Tue Mar 25, 2014  12:17PM
Last Modified : Sun Dec 11, 2016  05:36PM

Copyright (c) 2014 Tony R. McDaniel

Released under the terms of the GNU General Public License.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import division
import numpy as np
# from math import sqrt,cos,sin,pi

def euler_2d_mms(nodes,constants=None):
    from numpy import sqrt,cos,sin,pi,vstack,hstack
    x = nodes[:,0]
    y = nodes[:,1]

    if constants is None:
        # constants
        Gamma  = 1.4
        L      = 20.
        a_px   = 1.
        a_py   = 1.
        a_rhox = 1.
        a_rhoy = 1.
        a_ux   = 1.
        a_uy   = 1.
        a_vx   = 1.
        a_vy   = 1.
        p_0    = 0.
        p_x    = 1.
        p_y    = 1.
        rho_0  = 0.
        rho_x  = 1.
        rho_y  = 1.
        u_0    = 0.
        u_x    = 1.
        u_y    = 1.
        v_0    = 0.
        v_x    = 1.
        v_y    = 1.
    else:
        Gamma  = constants['Gamma']
        L      = constants['L']
        a_px   = constants['a_px']
        a_py   = constants['a_py']
        a_rhox = constants['a_rhox']
        a_rhoy = constants['a_rhoy']
        a_ux   = constants['a_ux']
        a_uy   = constants['a_uy']
        a_vx   = constants['a_vx']
        a_vy   = constants['a_vy']
        k      = constants['k']
        mu     = constants['mu']
        p_0    = constants['p_0']
        p_x    = constants['p_x']
        p_y    = constants['p_y']
        rho_0  = constants['rho_0']
        rho_x  = constants['rho_x']
        rho_y  = constants['rho_y']
        u_0    = constants['u_0']
        u_x    = constants['u_x']
        u_y    = constants['u_y']
        v_0    = constants['v_0']
        v_x    = constants['v_x']
        v_y    = constants['v_y']

    # auxilliary variables / exact solution
    PI   = pi
    Gm1  = Gamma - 1.0
    GGm1 = Gamma/Gm1
    RHO = rho_0 + rho_x * sin(a_rhox * PI * x / L) + rho_y * cos(a_rhoy * PI * y / L);
    U = u_0 + u_x * sin(a_ux * PI * x / L) + u_y * cos(a_uy * PI * y / L);
    V = v_0 + v_x * cos(a_vx * PI * x / L) + v_y * sin(a_vy * PI * y / L);
    P = p_0 + p_x * cos(a_px * PI * x / L) + p_y * sin(a_py * PI * y / L);
    E   = P/((Gamma-1)*RHO) + 0.5*(U**2 + V**2)

    # source terms
    Q_rho = a_rhox * PI * rho_x * U * cos(a_rhox * PI * x / L) / L - a_rhoy * PI * rho_y * V * sin(a_rhoy * PI * y / L) / L + (a_ux * u_x * cos(a_ux * PI * x / L) + a_vy * v_y * cos(a_vy * PI * y / L)) * PI * RHO / L;
    Q_u = a_rhox * PI * rho_x * U * U * cos(a_rhox * PI * x / L) / L - a_rhoy * PI * rho_y * U * V * sin(a_rhoy * PI * y / L) / L - a_uy * PI * u_y * RHO * V * sin(a_uy * PI * y / L) / L - a_px * PI * p_x * sin(a_px * PI * x / L) / L + (0.2e1 * a_ux * u_x * cos(a_ux * PI * x / L) + a_vy * v_y * cos(a_vy * PI * y / L)) * PI * RHO * U / L;
    Q_v = a_rhox * PI * rho_x * U * V * cos(a_rhox * PI * x / L) / L - a_rhoy * PI * rho_y * V * V * sin(a_rhoy * PI * y / L) / L - a_vx * PI * v_x * RHO * U * sin(a_vx * PI * x / L) / L + a_py * PI * p_y * cos(a_py * PI * y / L) / L + (a_ux * u_x * cos(a_ux * PI * x / L) + 0.2e1 * a_vy * v_y * cos(a_vy * PI * y / L)) * PI * RHO * V / L;
    Q_e = -a_px * PI * p_x * Gamma * U * sin(a_px * PI * x / L) / (Gamma - 0.1e1) / L + a_py * PI * p_y * Gamma * V * cos(a_py * PI * y / L) / (Gamma - 0.1e1) / L + (U * U + V * V) * a_rhox * PI * rho_x * U * cos(a_rhox * PI * x / L) / L / 0.2e1 - (U * U + V * V) * a_rhoy * PI * rho_y * V * sin(a_rhoy * PI * y / L) / L / 0.2e1 + (0.3e1 * a_ux * u_x * cos(a_ux * PI * x / L) + a_vy * v_y * cos(a_vy * PI * y / L)) * PI * RHO * U * U / L / 0.2e1 - (a_uy * u_y * sin(a_uy * PI * y / L) + a_vx * v_x * sin(a_vx * PI * x / L)) * PI * RHO * U * V / L + (a_ux * u_x * cos(a_ux * PI * x / L) + 0.3e1 * a_vy * v_y * cos(a_vy * PI * y / L)) * PI * RHO * V * V / L / 0.2e1 + (a_ux * u_x * cos(a_ux * PI * x / L) + a_vy * v_y * cos(a_vy * PI * y / L)) * PI * Gamma * P / (Gamma - 0.1e1) / L;
    exact = vstack((RHO, U, V, P)).T
    source = vstack(( Q_rho, Q_u, Q_v, Q_e )).T

    return exact,source


def mms_test():
    from solver import Solver
    from vtk_out import writeVTK
    from tools.constants import names_euler_2d_conserved
    from tools.conversions import conserved_to_primitive
    import numpy.linalg as la
    s = Solver('cases/mms_test.cfg')
    exact,source = euler_2d_mms(s.grid.nodes)
    print source
    print la.norm(source)
    s.add_source(source)
    s.step()
    q = conserved_to_primitive(s.Q)
    error = la.norm(q-exact)
    print("error = %f"%error)
    writeVTK("mms_test_out",s.grid,s.Q,names=names_euler_2d_conserved)
    writeVTK("mms_exact_out",s.grid,exact,names=names_euler_2d_conserved)

if __name__ == '__main__':
    mms_test()

