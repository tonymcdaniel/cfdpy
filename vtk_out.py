#!/usr/bin/env python
# encoding: utf-8
"""
File Name : vtk_out.py

Author    : Tony McDaniel
Contact   : tony@tonymcdaniel.com

Write grid and solution to VTK legacy file.

Creation Date : Wed Mar 12, 2014  04:48PM
Last Modified : Sun Dec 11, 2016  05:38PM

Copyright (c) 2014 Tony R. McDaniel

Released under the terms of the GNU General Public License.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from tools.logger import Logger

def writeVTK(filename,grid,solution,names=None,description=''):
    """
    Write the grid and solution variables to filename.vtk
    """
    g = Logger(prefix='VTK OUT : ')
    outfile = filename+'.vtk'
    g.log("Writing solution to "+outfile)
    with open(outfile,'w') as f:
        # write header
        f.write("# vtk DataFile Version 3.0")
        f.write("\n"+description)
        f.write("\nASCII")
        f.write("\nDATASET UNSTRUCTURED_GRID")

        # write points
        g.log("Writing %d points"%grid.nnode)
        f.write("\nPOINTS %d float"%grid.nnode)
        if grid.ndim == 2:
            for i in range(grid.nnode):
                f.write("\n%f %f 0.00000000"%(grid.nodes[i,0],grid.nodes[i,1]))
        else:
            for i in range(grid.nnode):
                f.write("\n%f %f %f"%(grid.nodes[i,0],grid.nodes[i,1],grid.nodes[i,2]))
        # write cells
        g.log("Writing %d cells"%grid.nelem)
        f.write("\n\nCELLS %d %d"%(grid.nelem, grid.nelem*(grid.npelem+1)));
        for i in range(grid.nelem):
            f.write("\n%d %d %d %d "%(grid.npelem,grid.elements[i,0],grid.elements[i,1],grid.elements[i,2]))
        f.write("\n");
        f.write("\nCELL_TYPES %d"%grid.nelem)
        for i in range(grid.nelem):
            f.write("\n5")
        # write point data
        g.log("Writing point data")
        f.write("\n\nPOINT_DATA %d"%grid.nnode)
        # for i in range(len(solution)):
        try:
            nvars = solution.shape[1]
        except:
            nvars = 1
        # make sure there are enough names for all the variables
        if names is not None:
            assert len(names) == nvars
        for i in xrange(nvars):
            g.log("Writing variable %d"%i)
            if names is not None:
                f.write("\nSCALARS "+names[i]+" float 1")
            else:
                f.write("\nSCALARS variable%d float 1"%i)
            f.write("\nLOOKUP_TABLE default")
            for j in range(grid.nnode):
                f.write("\n%f"%solution[j,i])
    g.log("Finished")
    g.stop()

def main():
    import numpy as np
    from grid import Grid
    g = Grid('../grids/SimpleSquare.su2')
    s = np.random.random((g.nnode,2))
    n = ['rand1','rand2']
    writeVTK('vtk_test_out',g,s,n,'test data for vtk writer')

if __name__ == '__main__':
    main()

