#!/usr/bin/env python

def fluxVanLeer(Ql, Qr, nx, ny, gamma=1.4):
    """
    Compute the flux of $Q$ across the boundary with unit normal $n$
    """
    from numpy import array, sqrt, zeros
    # normalize the normal vector
    length = sqrt(nx**2+ny**2)
    nx = nx/length
    ny = ny/length
    # values for left side
    rl = Ql[0]
    ul = Ql[1]/rl
    vl = Ql[2]/rl
    el = Ql[3]  # e is J per unit volume
    pl = (gamma-1.)*(el - 0.5*rl*(ul**2+vl**2))
    cl = sqrt(gamma*pl/rl)
    Ul = (ul*nx + vl*ny)
    # values for left side
    rr = Qr[0]
    ur = Qr[1]/rr
    vr = Qr[2]/rr
    er = Qr[3]  # e is J per unit volume
    pr = (gamma-1.)*(er - 0.5*rr*(ur**2+vr**2))
    cr = sqrt(gamma*pr/rr)
    Ur = (ur*nx + vr*ny)
    if Ul > cl:
        Fm = zeros(4)
        f1 = rl*Ul
        Fp = length*array([f1, f1*ul+pl*nx, f1*vl+pl*ny, (el+pl)*Ul])
    elif Ur < -cr:
        Fp = zeros(4)
        f1 = rr*Ur
        Fm = length*array([f1, f1*ur+pr*nx, f1*vr+pr*ny, (er+pr)*Ur])
    else:
        f1p = 0.25*rl*cl*((Ul/cl)+1.)**2
        f2p = -Ul+2.*cl
        f1m = -0.25*rr*cr*((Ur/cr)-1.)**2
        f2m = -Ur-2.*cr
        Fp = length*array([f1p,
                           f1p*(nx*f2p/gamma+ul),
                           f1p*(ny*f2p/gamma+vl),
                           f1p*((f2p*Ul*(gamma-1.)+2.*cl**2)/(gamma**2-1.)+0.5*(ul**2+vl**2))])
        Fm = length*array([f1m,
                           f1m*(nx*f2m/gamma+ur),
                           f1m*(ny*f2m/gamma+vr),
                           f1m*((f2m*Ur*(gamma-1.)+2.*cr**2)/(gamma**2-1.)+0.5*(ur**2+vr**2))])
    return Fp, Fm


def jacobianFD(Ql, Qr, nx, ny, gamma=1.4, dQ=1e-5):
    from numpy import array, sqrt, zeros
    Jp = zeros((4, 4))
    Jm = zeros((4, 4))
    Fp,Fm = fluxVanLeer(Ql, Qr, nx, ny)
    for j in range(4):
        Qlp = Ql.copy()
        Qrp = Qr.copy()
        Qlp[j] += dQ
        Qrp[j] += dQ
        dFp, dFm = fluxVanLeer(Qlp, Qrp, nx, ny)
        Jp[:, j] = (dFp - Fp)/dQ
        Jm[:, j] = (dFm - Fm)/dQ
    return Jp, Jm


