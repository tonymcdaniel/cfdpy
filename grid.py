#!/usr/bin/env python
# encoding: utf-8
"""
File Name : grid.py

Author    : Tony McDaniel
Contact   : tony@tonymcdaniel.com

Generic unstructured 2D grid class for finite volume or finite element problems.

Creation Date : Wed Feb 12, 2014  10:32AM
Last Modified : Sun Dec 11, 2016  05:36PM

Copyright (c) 2014 Tony R. McDaniel

Released under the terms of the GNU General Public License.

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
from __future__ import division
import numpy as np
import numpy.linalg as la
import matplotlib.pyplot as plt

npelem = {'tri':3,'quad':4,'tet':4,'hex':8,'prism':6,'pyramid':5}

class Grid(object):

    """Parent class for grid objects"""

    def __init__(self,infile=None):
        self.ndim       = 0
        self.nelem      = 0
        self.npelem     = 3
        # self.elements   = {}
        self.nnode      = 0
        self.nodes      = None
        self.nbound     = 0
        self.boundaries = {}
        self.area       = None
        if infile:
            self.read(infile)

    def nelem(self):
        return self.nelem

    def nnodes(self):
        return self.nnode

    def get_nodes(self):
        return self.nodes

    def get_node(self,node):
        return self.nodes[node,:]

    def get_elements(self):
        return self.elements

    def get_element(self,element_id):
        return self.elements[element_id]

    def list_nodes(self):
        print("Nodes :")
        for i in range(self.nnode):
            print(" %d  % 5.3f  % 5.3f"%(i,self.nodes[i,0],self.nodes[i,1]))

    def list_elements(self):
        print("Elements :")
        for i in range(self.nelem):
            print(" %d  %d  %d  %d"%(i,self.elements[i,0],self.elements[i,1],self.elements[i,2]))

    def compute_metrics(self):
        # Triangle areas
        tri = self.nodes[self.elements]
        a = 0.5*np.cross(tri[::,1]-tri[::,0],tri[::,2]-tri[::,0])
        print("GRID METRICS : Min Area = %e"%a.min())
        print("GRID METRICS : Max Area = %e"%a.max())
        print("GRID METRICS : Avg Area = %e"%a.mean())
        if a.min()<0.0:
            print("GRID METRICS : Negative areas encountered. Checking winding.")
            self.fix_winding(a)
        if self.area is None:
            print("GRID METRICS : Saving control volume areas.")
            self.area = np.zeros(self.nnode)
            for i in range(self.nelem):
                self.area[self.elements[i,0]] += a[i]/3.
                self.area[self.elements[i,1]] += a[i]/3.
                self.area[self.elements[i,2]] += a[i]/3.
        self.check_boundary_winding()


    def fix_winding(self,a):
        count = 0
        for i in range(self.nelem):
            t = self.nodes[self.elements[i]]
            if a[i] < 0.0:
                e = self.elements[i,0]
                self.elements[i,0] = self.elements[i,2]
                self.elements[i,2] = e
                a[i] *= -1.
                count += 1
        print("GRID METRICS : Fixed winding for %d triangles."%count)
        self.compute_metrics()

    def check_boundary_winding(self):
        for tag,elm in self.boundaries.iteritems():
            for e in elm:
                pl = e[0]
                pr = e[1]
                n = np.array([self.nodes[pr,1] - self.nodes[pl,1],-(self.nodes[pr,0] - self.nodes[pl,0])])
                m = 0.5*(self.nodes[pl]+self.nodes[pr])
                d = np.dot(n,m-self.grid_center)
                if tag == 'freestream' and d < 0.0:
                    # boundary is reversed
                    e[0] = pr
                    e[1] = pl
                    print("Reversing freestream boundary (%d,%d)"%(pl,pr))
                if tag == 'wall' and d > 0.0:
                    # boundary is reversed
                    e[0] = pr
                    e[1] = pl
                    print("Reversing wall boundary (%d,%d)"%(pl,pr))


    def compute_normals(self):
        self.normals = np.zeros((self.nelem,3,2))
        tri = self.nodes[self.elements]
        centers = np.average(tri,axis=1)
        midpoints = 0.5*np.array([tri[::,0]+tri[::,1],tri[::,1]+tri[::,2],tri[::,2]+tri[::,0]])
        self.normals[:,:,0] = (centers[:,1] - midpoints[:,:,1]).T
        self.normals[:,:,1] = (midpoints[:,:,0] - centers[:,0]).T


    def plot(self,outfile=None):
        """
        Display the grid on screen or write to outfile.
        """
        pass


    def read(self,gridfile,gridtype=None):
        """
        Read grid data from gridfile.
        """
        if gridtype is None:
            # try to guess based on file extension
            ext = gridfile.split('.')[-1]
            if ext == 'grd':
                self._read_tetrex(gridfile)
            elif ext == 'msh':
                self._read_gmesh(gridfile)
            elif ext == 'mesh':
                self._read_mesh(gridfile)
            elif ext == 'su2':
                self._read_su2(gridfile)
            else:
                error_msg("Grid "+gridfile+" is an unknown format.",1)
        elif gridtype == 'tetrex':
            self._read_tetrex(gridfile)
        elif gridtype == 'mesh':
            self._read_mesh(gridfile)
        elif gridtype == 'gmsh':
            self._read_gmesh(gridfile)
        elif gridtype == 'su2':
            self._read_su2(gridfile)
        else:
            error_msg("Unable to read "+gridfile+".",1)
        self.compute_metrics()
        self.compute_normals()


    def write(self,gridfile,gridtype='su2'):
        """
        Read grid data from gridfile.
        """
        ext = gridfile.split('.')[-1]
        if gridtype is None:
            # try to guess based on file extension
            if ext == 'grd':
                self._write_tetrex(gridfile)
            elif ext == 'msh':
                self._write_gmesh(gridfile)
            elif ext == 'su2':
                self._write_su2(gridfile)
            else:
                error_msg("Grid "+gridfile+" is an unknown format.",1)
        elif gridtype == 'su2':
            if ext != 'su2':
                gridfile = gridfile+'.su2'
            self._write_su2(gridfile)
        else:
            error_msg("Unable to write "+gridfile+".",1)


    def _read_tetrex(self,gridfile):
        """
        Read grid data in ASCII Tetrex format.
        """
        print("WARNING : THIS HAS NOT BEEN TESTED")
        return 1
        with open(gridfile,'r') as f:
            f.readline() # 1
            f.readline() # Tetrex Grid File exported by Pointwise
            f.readline() # 1
            f.readline() # ndimn    nzone   npoin      nvp
            data = f.readline().split()
            self.ndim = int(data[0])
            self.nnode = int(data[2])
            self.nodes = np.zeros((self.nnode,self.ndim))
            f.readline() # zone name     ncell   nbsurf
            data = f.readline().split()
            self.nelem = int(data[1])
            self.nbedge = int(data[2])
            self.elements = np.zeros((self.nelem,3))
            f.readline() # volume number   volume type    point list
            for i in range(self.nelem):
                data = f.readline().split()
                self.elements[i,0] = int(data[2])-1
                self.elements[i,1] = int(data[3])-1
                self.elements[i,2] = int(data[4])-1
            f.readline() # coordinates
            for i in range(self.nnode):
                data = f.readline().split()
                self.nodes[i,0] = float(data[0])
                self.nodes[i,0] = float(data[0])
            f.readline() # bc name   volume number   local surface number
            for i in range(self.nbedge):
                data = f.readline().split()
                if data[0] in self.boundary.keys():
                    self.boundary[data[0]].append((int(data[1])-1,int(data[2])-1))
                else:
                    self.boundary[data[0]] = [(int(data[1])-1,int(data[2])-1)]


    def _read_gmesh(self,gridfile):
        print("WARNING : THIS HAS NOT BEEN TESTED")
        return 1
        with open(gridfile,'r') as f:
            f.readline() # $MeshFormat
            f.readline() # 2.2 0 8
            f.readline() # $EndMeshFormat
            f.readline() # $Nodes
            nnodes = int(f.readline().split()[0])
            self.nodes = np.zeros((nnodes,4))
            for i in range(nnodes):
                data = f.readline().split(' ')
                self.nodes[i,0] = int(data[0])   # node id
                self.nodes[i,1] = float(data[1]) # x coord
                self.nodes[i,2] = float(data[2]) # y coord
                self.nodes[i,3] = float(data[3]) # z coord
            f.readline() # $EndNodes
            f.readline() # $Elements
            nelem = int(f.readline().split()[0])
            self.elements = np.zeros((nelem,4))
            for i in range(nelem):
                data = f.readline().split(' ')
                self.elements[i,0] = int(data[0])   # elem id
                self.nodes[i,1] = float(data[5]) # node 1
                self.nodes[i,2] = float(data[6]) # node 2
                self.nodes[i,3] = float(data[7]) # node 3


    def _read_mesh(self,gridfile):
        self.ndim = 2
        with open(gridfile,'r') as f:
            print("GRID IO: Reading data from "+gridfile)
            print("GRID IO: Number of dimensions = "+str(self.ndim))
            f.readline() # header
            self.nnode = int(f.readline().split()[0])
            self.nodes = np.zeros((self.nnode,self.ndim))
            print("GRID IO: Number of nodes = "+str(self.nnode))
            for i in range(self.nnode):
                data = f.readline().split()
                self.nodes[i,0] = float(data[0])
                self.nodes[i,1] = float(data[1])
                if self.ndim == 3:
                    self.nodes[i,2] = float(data[2])
            self.grid_center = np.average(self.nodes,axis=0)
            f.readline() # Number of blocks
            f.readline() # 1
            f.readline() # header
            self.nelem = int(f.readline().split()[0])
            self.elements = np.zeros((self.nelem,3),dtype=np.int)
            print("GRID IO: Number of elements = "+str(self.nelem))
            for i in range(self.nelem):
                data = f.readline().split()
                self.elements[i,0] = int(data[0])-1
                self.elements[i,1] = int(data[1])-1
                self.elements[i,2] = int(data[2])-1
            f.readline() # Number of quads
            f.readline() # 0
            f.readline() # header
            self.nbound = int(f.readline().split()[0])
            print("GRID IO: Number of boundaries = "+str(self.nbound))
            if self.nbound > 0:
                for i in range(self.nbound):
                    tag = f.readline().split()[1]
                    if tag == 'Farfield':
                        tag = 'freestream'
                    if tag == 'Inviscid_Wall':
                        tag = 'wall'
                    nel = int(f.readline().split()[0])
                    elm = np.zeros((nel,2),dtype=np.int)
                    for j in range(nel):
                        data = f.readline().split()
                        elm[j,0] = int(data[0])-1
                        elm[j,1] = int(data[1])-1
                    self.boundaries[tag] = elm
            print("GRID IO: Finished reading grid.")



    def _read_su2(self,gridfile):
        with open(gridfile,'r') as f:
            print("GRID IO: Reading data from "+gridfile)
            line = f.readline()
            while line:
                first = line.split()[0]
                if first != '%':
                    if first == 'NDIME=':
                        self.ndim = int(line.split()[1])
                        print("GRID IO: Number of dimensions = "+str(self.ndim))
                    if first == 'NELEM=':
                        self.nelem = int(line.split()[1])
                        print("GRID IO: Number of elements = "+str(self.nelem))
                        self.elements = np.zeros((self.nelem,3),dtype=np.int)
                        for i in range(self.nelem):
                            data = f.readline().split()
                            self.elements[i,0] = int(data[1])
                            self.elements[i,1] = int(data[2])
                            self.elements[i,2] = int(data[3])
                    if first == 'NPOIN=':
                        self.nnode = int(line.split()[1])
                        print("GRID IO: Number of nodes = "+str(self.nnode))
                        self.nodes = np.zeros((self.nnode,self.ndim))
                        for i in range(self.nnode):
                            data = f.readline().split()
                            self.nodes[i,0] = float(data[0])
                            self.nodes[i,1] = float(data[1])
                            if self.ndim == 3:
                                self.nodes[i,2] = float(data[2])
                    if first == 'NMARK=':
                        self.nbound = int(line.split()[1])
                        print("GRID IO: Number of boundaries = "+str(self.nbound))
                        if self.nbound > 0:
                            for i in range(self.nbound):
                                tag = f.readline().split()[1]
                                nel = int(f.readline().split()[1])
                                elm = np.zeros((nel,2),dtype=np.int)
                                for j in range(nel):
                                    data = f.readline().split()
                                    elm[j,0] = int(data[1])
                                    elm[j,1] = int(data[2])
                                self.boundaries[tag] = elm
                line = f.readline()
            self.grid_center = np.average(self.nodes,axis=0)
            print("GRID IO: Finished reading grid.")


    def _write_su2(self,gridfile):
        with open(gridfile,'w') as f:
            print("GRID IO: Writing data to "+gridfile)
            f.write("% \n% Problem dimension \n%\n")
            f.write("NDIME= %d\n"%self.ndim)
            f.write("%\n% Inner element connectivity\n%\n")
            f.write("NELEM= %d\n"%self.nelem)
            print("GRID IO: Writing elements ...")
            for i in range(self.nelem):
                e = self.get_element(i)
                f.write(" 5     %d    %d    %d    %d\n"%(e[0],e[1],e[2],i))
            f.write("%\n% Node coordinates\n%\n")
            f.write("NPOIN= %d\n"%self.nnode)
            print("GRID IO: Writing nodes ...")
            for i in range(self.nnode):
                e = self.get_node(i)
                f.write("     %.17f    %.17f    %d\n"%(e[0],e[1],i))
            print("GRID IO: Writing boundaries ...")
            f.write("%\n% Boundary elements\n%\n")
            f.write("NMARK= %d\n"%self.nbound)
            for k,e in self.boundaries.iteritems():
                f.write("MARKER_TAG= %s\n"%k)
                f.write("MARKER_ELEMS= %d\n"%(len(e)))
                for i in range(len(e)):
                    f.write(" 3     %d     %d\n"%(e[i,0],e[i,1]))
            print("GRID IO: Finished writing grid.")




class Grid2D(Grid):

    """Class for 2D unstructured grids."""

    def __init__(self):
        """@todo: to be defined1. """
        Grid.__init__(self)


    def compute_metrics(self):
        # Triangle areas
        tri = self.nodes[self.elements]
        self.centroid = (tri[::,0] + tri[::,1] + tri[::,2])/3. # triangle centroid
        self.midpoints = 0.5*np.array([tri[::,0]+tri[::,1],tri[::,1]+tri[::,2],tri[::,2]+tri[::,0]])  # edge midpoints
        self.int_normals = np.dot([[0,1],[-1,0]],(self.centroid - self.midpoints).T).T  # interior normals
        a = 0.5*np.cross(tri[::,1]-tri[::,0],tri[::,2]-tri[::,0])
        print("GRID METRICS : Min Area = %f  Max Area = %f  Avg Area = %f"%(a.min(),a.max(),a.mean()))


    def initialize(self):
        self.centroid = (p0 + p1 + p2)/3. # triangle centroid
        self.midpoints = 0.5*np.array([p0+p1,p1+p2,p2+p0])  # edge midpoints
        self.normals = np.array([[p1[1]-p0[1],p0[0]-p1[0]], [p2[1]-p1[1],p1[0]-p2[0]], [p0[1]-p2[1],p2[0]-p0[0]]])  # face normals
        self.edge_lengths = np.sqrt(np.sum(self.normals**2,axis=-1))  # face lengths
        self.int_normals = np.dot([[0,1],[-1,0]],(self.centroid - self.midpoints).T).T  # interior normals



def main():
    g = Grid('../grids/SimpleSquare.su2')
    g.write('../grids/SimpleSquareOut.su2')

if __name__ == '__main__':
    main()

